FROM masterthemainframe/ansible:latest

# Env for Ansible and Git
ENV ANSIBLE_USER="z01752"
ENV ANSIBLE_PASSWORD=""
ENV ANSIBLE_PATH="/ansible"
ENV GIT_PATH="/git"

LABEL maintainer="mathieu.escribe@labanquepostale.fr"

# install git
RUN yum install -y git

# ssh config
RUN  mkdir /root/.ssh && \
     chmod 0700 /root/.ssh && \
     chown root:root /root/.ssh
ADD .ssh/id_rsa /root/.ssh/
RUN  chmod 0600 /root/.ssh/id_rsa && \
     chown root:root /root/.ssh/id_rsa
ADD .ssh/id_rsa.pub /root/.ssh/
RUN chmod 0644 /root/.ssh/id_rsa.pub && \
     chown root:root /root/.ssh/id_rsa.pub

# ansible deploy files
RUN  mkdir "$ANSIBLE_PATH" && \
     chmod 0775 "$ANSIBLE_PATH"
COPY ansible.cfg "$ANSIBLE_PATH"
COPY inventory "$ANSIBLE_PATH"
COPY templates "$ANSIBLE_PATH"/templates/
COPY group_vars "$ANSIBLE_PATH"/group_vars/
COPY app.yml "$ANSIBLE_PATH"

# git create directory
RUN  mkdir "$GIT_PATH" && \
     chmod 0775 "$GIT_PATH"
ADD .gitconfig /root/
RUN chmod 0644 /root/.gitconfig && \
    chown root:root /root/.gitconfig

WORKDIR $ANSIBLE_PATH

# ENTRYPOINT Containers Running
ENTRYPOINT ["/usr/local/bin/ansible-playbook", "-i", "inventory", "app.yml"]

